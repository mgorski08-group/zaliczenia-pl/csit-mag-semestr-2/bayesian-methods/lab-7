import arviz as az
import matplotlib.pyplot as plt
import numpy as np
import pymc3 as pm

yes_ans = 5

with pm.Model() as model:
    p_d = pm.Uniform("p_d", lower=0, upper=1)
    p_observed = pm.Deterministic("p_observed", 0.25 + 0.5*p_d)

    occurrences = np.concatenate((np.zeros(100-yes_ans), np.ones(yes_ans)))
    answers = pm.Bernoulli("answers", p_observed, observed=occurrences)

    idata = pm.sample(2000, tune=2500)

az.plot_trace(idata)
plt.gcf().suptitle(f"Yes answers = {yes_ans}")
plt.tight_layout()
plt.show()

# For all 3 examples (20, 10, 5 yes answers) the most likely probability
# for the person taking drugs is 0. The difference is in the confidence
# where the model is the most confident with 5 yes and 95 no answers.
# All 3 possibilities are quite unlikely, because for the situation of
# zero people taking drugs, we expect the probability of yes answer
# to be 0.25 (0.25 + 0.5*0). When we observe the frequency of yes answers
# below 0.25 it is suspicious, as we expect around 25 yes answers per 100
# just from the coin tosses. It can mean that people didn't follow the instructions
# correctly, the coin could be not fair or the result happened just
# by chance - 100 trials is not that big of a sample.
